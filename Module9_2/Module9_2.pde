Mover mover;
Mover[] movers = new Mover[5];

PVector wind = new PVector(0.07f, 0);

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  noStroke();

  for (int i = 0; i < movers.length; i ++) {
    float m = 1;
    float s = 20;
    float mRand = 0;

    mRand = random(3, 10);
    m *= mRand;

    movers[i] = new Mover();
    movers[i].position.x = Window.left + 50;
    movers[i].position.y = (Window.top - 150) - (200 * i);

    movers[i].mass = m;
    movers[i].scale = s;
    movers[i].setColor(random(255), random(255), random(255), 255);
  }
}

void draw() {
  background(0);

  for (int i = 0; i < movers.length; i++) {
    movers[i].render();
    movers[i].update();

    if (movers[i].position.x < ((Window.left + Window.right) / 2)) {
      movers[i].applyForce(wind);
    }
    if (movers[i].position.x >= ((Window.left + Window.right) / 2)) {
      movers[i].applyFriction();
    }
    if (frameCount > 900) {
      frameCount = -1;
    }
  }
}
