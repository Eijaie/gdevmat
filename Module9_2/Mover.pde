public class Mover{
 public PVector position = new PVector();
 public PVector velo = new PVector();
 public PVector accele = new PVector();
 
 public float mass = 1;
 public float scale = 20;
 public float red = 255, green = 255, blue = 255, alpha = 255;
 
  Mover(){
    
  }
  
  Mover (float x, float y){
    position = new PVector(x, y);
   }
  
  Mover (float x, float y, float scale){
   position = new PVector(x, y);
   this.scale = scale;
  }
  
  Mover (PVector position){
   this.position = position; 
  }
  
  public void render(){
   fill (red, green, blue, alpha);
   circle (position.x, position.y, this.mass * 10);
  }
  
  public void setColor (float r, float g, float b, float a){
   this.red = r;
   this.green = g;
   this.blue = b;
   this.alpha = a;
  }
  
  public void update(){
   this.velo.add(this.accele);
   this.velo.limit(25);
   
   this.position.add(this.velo);
   this.accele.mult(0);
  }
  
  public void applyForce(PVector force){
   PVector a = PVector.div(force, this.mass);
   this.accele.add(a);
  }
  
  public void applyFriction(){
   float c = 0.1;
   float normal = 1;
   float frictionMag = c * normal;
   PVector friction = this.velo.copy();
   
   friction.mult(-1);
   friction.normalize();
   friction.mult(frictionMag);
   applyForce(friction);
  }
}
