Mover mover;
Mover[] movers = new Mover[10];
Liquid ocean = new Liquid(0, -100, Window.right, Window.bottom, 0.1f);
PVector wind = new PVector(0.9f, 0);

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  noStroke();

  for (int i = 0; i < movers.length; i++) {
    float m = 13;
    movers[i] = new Mover();
    movers[i].position.y = 450;
    movers[i].position.x = (Window.left + 150) + (180 * i);

    m -= i;
    movers[i].mass = m;
    movers[i].setColor(random(255), random(255), random(255), 255);
  }
}

void draw() {
  background(0);
  ocean.render();

  for (int i = 0; i < movers.length; i++) {
    movers[i].render();
    movers[i].update();

    movers[i].applyGravity();

    if (movers[i].position.x > Window.right) {
      movers[i].velo.x *= -1;
      movers[i].position.x = Window.right;
    }
    if (movers[i].position.y < Window.bottom) {
      movers[i].velo.y *= -1;
      movers[i].position.y = Window.bottom;
    }

    if (ocean.isCollidingWith(movers[i])) {
      movers[i].applyForce(ocean.calculateDragForce(movers[i]));
    } else {
      movers[i].applyForce(wind);
    }
  }

  if (frameCount == 250) {
    frameCount = -1;
  }
}
