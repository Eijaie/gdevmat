public class Mover
{
  public PVector position = new PVector();
  public PVector velo = new PVector();
  public PVector accele = new PVector();
  
  public float mass = 1;
  public float scale = 20;
  public float red = random(255), green = random(255), blue = random(255), alpha = 255;
  
  public void setColor(float r, float g, float b, float a){
   this.red = r;
   this.green = g;
   this.blue = b;
   this.alpha = a;
  }
  
  Mover(){
    
  }
  
  Mover (float x, float y){
   position = new PVector(x, y); 
  }
  
  Mover (float x, float y, float scale){
   position = new PVector(x, y);
   this.scale = scale;
  }
  
  Mover (PVector position){
   this.position = position;
   velo = new PVector();
  }
  
  Mover (PVector position, float scale){
   velo = new PVector();
   this.position = position;
   this.scale = scale;
  }
  
  public void render(){
   fill (red, green, blue, alpha);
   circle (position.x, position.y, scale);
  }
  
  public void update(){
   this.velo.add(this.accele);
   this.velo.limit(35);
   
   this.position.add(this.velo);
   
   this.accele.mult(0);
  }
  
  public void applyForce(PVector force){
   PVector a = PVector.div(force, this.mass);
   this.accele.add(a);
  }
}
