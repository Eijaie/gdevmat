Mover[] mover = new Mover[10];
PVector wind = new PVector(0.08f, 0);
PVector gravity = new PVector(0, -0.6);

void setup()
{
  size(1920, 1080, P3D);
  camera(0, 0, Window.eyeZ, 0, 0, 0, 0, -1, 0);
  noStroke();
  for (int i = 0; i < mover.length; i++) {
    float m = 1;
    float s = 20;

    m *= i;
    s *= i;
    mover[i] = new Mover();
    mover[i].position.x = Window.left + 25;
    mover[i].mass = m;
    mover[i].scale = s;
    mover[i].setColor(random(255), random(255), random(255), 255);
  }
}

void draw() {
  background(0);

  for (int i = 0; i < mover.length; i++) {

    mover[i].render();
    mover[i].update();

    mover[i].applyForce(wind);
    mover[i].applyForce(gravity);

    if (mover[i].position.y < Window.bottom) {
      mover[i].velo.y *= -1;
      mover[i].position.y = Window.bottom;
    }

    if (mover[i].position.x > Window.right) {
      mover[i].velo.x *= -1;
      mover[i].position.x = Window.right;
    }
  }
}
