blackHole center = new blackHole();
Matter[] otherMatter = new Matter[150];

class blackHole{
  PVector bhPos = new PVector();
  float xPos, yPos;
  
  void position(){
   xPos = random(Window.left + 100, Window.right - 100);
   yPos = random(Window.bottom + 100, Window.top - 100);
   bhPos = new PVector(xPos, yPos);
  }
  
  void render(){
    fill(255);
    circle(xPos, yPos, 50);
  }
}

class Matter{
  PVector coords = new PVector();
  
  float radius = random(10, 45);
  float speed = 6;
  
  color mattCol = color(random(255), random(255), random(255), 155);
  
   void direct(PVector bhPos){
     PVector direction = PVector.sub(bhPos, coords);
     direction.normalize();
     direction.mult(speed);
     coords.add(direction);
   }
   
   void randomize(){
    float xgauss = randomGaussian();
    float ygauss = randomGaussian();
    float stanDev = 350;
    float mean = 80;
    
    coords = new PVector((stanDev * xgauss) + mean, ((stanDev - 100) * ygauss) + (mean - 30));
   }
   
   void render(){
    fill(mattCol);
    circle(coords.x, coords.y, radius);
   }
}

void setup(){
 size(1920, 1080, P3D);
 camera(0, 0, -(height/2.0) / tan(PI * 30/180), 0, 0, 0, 0, -1, 0);
 background(0);
 noStroke();
 
 for(int i = 0; i < otherMatter.length; i++){
  otherMatter[i] = new Matter();
  otherMatter[i].randomize(); 
 }
}

void draw(){
  clear();
  for(int i = 0; i < otherMatter.length; i++){
    otherMatter[i].render();
    otherMatter[i].direct(center.bhPos); 
  }
  
  if(frameCount % 200 == 0){
    center.position();
    for(int i = 0; i < otherMatter.length; i++){
     otherMatter[i].randomize();
    }
  }
  
  center.render();
}
