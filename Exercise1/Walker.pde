class Walker {
 float xPosition;
 float yPosition;
 
 void render() {
  circle(xPosition, yPosition, 50); 
 }
 
 void randomWalk(){
   int decision = floor(random(8));
   
   if (decision == 0){
    yPosition += random(30);
   }
   else if (decision == 1){
     yPosition -= random(30);
   }
   else if (decision == 2){
    xPosition -= random(30); 
   }
   else if (decision == 3){
    xPosition += random(30); 
   }
   else if (decision == 4){
    yPosition += random(30);
    xPosition += random(30);
   }
   else if (decision == 5){
    yPosition += random(30);
    xPosition -= random(30);
   }
   else if (decision == 6){
    yPosition -= random(30);
    xPosition += random(30);
   }
   else if (decision == 7){
    yPosition -= random(30);
    xPosition -= random(30);
   }
 }
}
