void setup(){
 size (1920, 1080, P3D);
 camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180),
 0, 0, 0,
 0, -1, 0);
 background(0);
}

Walker walker = new Walker();
Walker walker2 = new Walker();
Walker walker3 = new Walker();
void draw() {
  fill(random(255), random(255), random(255), random(150));
  walker.render();
  walker2.render();
  walker3.render();
  
  walker.randomWalk();
  walker2.randomWalk();
  walker3.randomWalk();
}
