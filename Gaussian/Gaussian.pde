void setup(){
 size(1920, 1080, P3D);
 camera(0, 0, -(height/2.0) / tan(PI * 30/180),
        0, 0, 0,
        0, -1, 0);
        background(0);
}

void draw(){
  if(frameCount % 1000 == 0){
   background(0); 
  }
  fill(random(255), random(255), random(255), random(10, 100));
  
  float gauss = randomGaussian();
  float standardDeviation = 350;
  float mean = 10;
  
  float gauss2 = randomGaussian();
  float standardDeviation2 = 50;
  float mean2 = 0;
  
  float xPos = (standardDeviation * gauss) + mean;
  float paintSize = (standardDeviation2 * gauss2) + mean2;
  
  noStroke();
  circle(xPos, random(-450, 450), paintSize);
 
}
