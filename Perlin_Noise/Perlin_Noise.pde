void setup(){
 size (1920, 1080, P3D);
 camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180),
 0, 0, 0,
 0, -1, 0);
 background(0);
}

float[] deltaColor = new float[4];
float[] deltaPos = new float[3];

Walker walker = new Walker();

void draw() {
  fill(map(noise(deltaColor[0] + 55), 0f, 1f, 1f, 255f), map(noise(deltaColor[1] + 40), 0f, 1f, 1f, 255f),
  map(noise(deltaColor[2] + 30), 0f, 1f, 1f, 255f), map(noise(deltaColor[3] + 90), 0f, 1f, 40f, 180f));
  
  walker.render();
  walker.randomWalk(deltaPos);
  
  for(int i = 0; i < deltaColor.length; i++){
    deltaColor[i] += 0.01f;
  }
  for(int i = 0; i < deltaPos.length; i++){
   deltaPos[i] += 0.01f; 
  }
}
