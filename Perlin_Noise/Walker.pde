class Walker {
 float xPosition;
 float yPosition;
 float diameter;
 
 void render() {
  circle(xPosition, yPosition, diameter);
  noStroke();
 }
 
 void randomWalk(float[] time){
   float x = noise(time[0] + 20);
   float y = noise(time[1] + 50);
   
   xPosition = map(x, 0, 1, Window.left, Window.right);
   yPosition = map(y, 0, 1, Window.bottom, Window.top);
   
   diameter = map(noise(time[2] + 450), 0, 1, 66, 220);
 }
}
