void setup() {
 size (1920, 1080, P3D);
 camera(0, 0, -(height / 2.0) / tan(PI * 30 / 180), 0, 0, 0, 0, -1, 0);
 noStroke();
 smooth();
 filter(BLUR, 6);
}

Vector2 mousePos(){
 float x = mouseX - Window.windowWidth;
 float y = -(mouseY - Window.windowHeight);
 return new Vector2 (x, y);
}

float[] deltaColor = new float[5];
float angle;
void draw(){
  background(0);
  
  rotate(angle);
  angle = map(noise(deltaColor[4]), 0, 1, 0, 30);
  
  Vector2 mouse = mousePos().normalize().mult(-497);
  Vector2 mouse2 = mousePos().normalize().mult(497);
  strokeWeight(4);
  
  stroke(200, 200, 200, 255);
  line(0, 0, mouse.x, mouse.y);
  line(0, 0, mouse2.x, mouse2.y);
  
  Vector2 mouse5 = mousePos().normalize().mult(-500);
  Vector2 mouse6 = mousePos().normalize().mult(500);
  strokeWeight(10);
  stroke(map(noise(deltaColor[0] + 10), 0f, 1f, 1f, 255f), map(noise(deltaColor[1] + 25), 0f, 1f, 1f, 255f),
  map(noise(deltaColor[2] + 15), 0f, 1f, 1f, 255f), 200);
  line(0, 0, mouse5.x, mouse5.y);
  line(0, 0, mouse6.x, mouse6.y);
  
  Vector2 mouse3 = mousePos().normalize().mult(-80);
  Vector2 mouse4 = mousePos().normalize().mult(80);
  strokeWeight(15);
  
  stroke(150);
  line(0, 0, mouse3.x, mouse3.y);
  line(0, 0, mouse4.x, mouse4.y);
  
  println(mouse.mag());
  
  for(int i = 0; i < deltaColor.length; i++){
    deltaColor[i] += 0.01f;
  }
}
